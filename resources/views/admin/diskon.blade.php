@extends('admin.layout.main')
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Diskon</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/admin/diskon">Diskon</a></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  @if(session()->has('success'))
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script>
      Swal.fire({
          icon: 'success',
          title: 'Success!',
          text: '{{ session("success") }}',
      });
  </script>
  @endif
  <!-- /.content-header -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#Hapusmulti">
              Hapus
            </button>
            </div>
          <div class="modal fade" id="Hapusmulti" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Hapus Data Diskon </h5>
                  <span type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">&times;</span>
                </div>
                <div class="modal-body">
                  <h3>Apakah anda Yakin ingin Menghapus </h3><h3 id="checkedCount">0</h3><h3> baris Ini?</h3>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" onclick="submitmulti()">Hapus</button>
                </div>
              </div>
            </div>
          </div>
          <form action="/admin/diskon/multidel" id="myform" method="POST">
            @csrf
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
      <thead>
          <tr align="left">
            <th scope="col"><input type="checkbox" class="check-all"></th>
              <th scope="col">No</th>
              <th scope="col">Kode Pendaftaran</th>
              <th scope="col">Email</th>
              <th scope="col">Bukti Diskon</th>
              <th scope="col">Jenis Diskon</th>
              <th scope="col">Jumlah Diskon</th>
              <th scope="col">Aksi</th>
          </tr>
      </thead>
      <tbody>
        <?php $no= 1; ?>
          @foreach ($data as $row)
          <tr>
            <td>
              <input type="checkbox" name="ids[{{ $row->id }}]" value="{{ $row->id }}">
            </td>
          </form>
          
              <th>{{ $no++}}</th>
              <td>{{  $row->user->kode_pendaftaran }}</td>
              <td>{{$row->user->email}}</td>
              <td><img id="myImg{{$row->id}}" src="{{ asset('bukti_diskon/'.$row->bukti_diskon) }}" alt="" width="70px" height="80px"></td>
              <td>{{$row->jenis_diskon}}</td>
              <td><span class="rupiah-format" data-amount="{{ $row->jumlah_diskon }}">Rp. <span class="amount">{{ number_format($row->jumlah_diskon, 0, ',', '.') }}</span></span></td>
              <td>
                <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#EditDiskon{{$row->id}}">
                  Edit
                </button>
                <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#HapusDiskon{{$row->id}}">
                  Hapus
                </button>
              </td>
          </tr>
          <!-- Modal for the first image -->
          <div id="modal{{$row->id}}" class="modal-preview">
                      
            <span class="close" id="close{{$row->id}}">&times;</span>
            <img class="modal-image" id="modalImg{{$row->id}}" width="150px" height="300px">

          </div>

  <div class="modal fade" id="EditDiskon{{$row->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Data Diskon </h5>
          <span type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">&times;</span>
        </div>
        <div class="modal-body">
          <form action="/admin/diskon/update/id={{ $row->id}}" method="POST" enctype="multipart/form-data"id="edit">
            @csrf
            <input type="hidden" name="id"/>
            <div class="mb-3">
              @csrf
                <label class="form-label">Bukti Diskon</label>
                <input accept="image/*" type='file' name="bukti_diskon" id="imgInp" />
                <img id="blah" alt="your image" src="{{ asset('bukti_diskon/'.$row->bukti_diskon) }}" width="450px" />
                <label class="form-label">Jenis Diskon</label>
                <select name="jenis_diskon" class="form-control">
                  <option value="Tidak Punya" {{ $row->jenis_diskon === 'Tidak Punya' ? 'selected' : '' }}>Tidak Punya</option>
                  <option value="STC" {{ $row->jenis_diskon === 'STC' ? 'selected' : '' }}>STC (Rp.800.000)</option>
                  <option value="Juara Umum" {{ $row->jenis_diskon === 'Juara Umum' ? 'selected' : '' }}>Juara Umum Dari Kelas 1 Sampai Kelas 3 Dan Seringkat 1 Sampai 3 (Rp.1.200.000)</option>
                  <option value="Juara Kelas" {{ $row->jenis_diskon === 'Juara Kelas' ? 'selected' : '' }}>Juara Kelas Dari Kelas 1 Sampai Kelas 3 Dan Seringkat 1 Sampai 3 (Rp.800.000)</option>
                  <option value="Daerah Kuta Selatan" {{ $row->jenis_diskon === 'Daerah Kuta Selatan' ? 'selected' : '' }}>Daerah Kuta Selatan (Rp.1.000.000)</option>
              </select>              
              <label class="form-label" id="labelJumlah{{$row->id}}">Jumlah Diskon: <span class="rupiah-format" data-amount="{{ $row->jumlah_diskon }}">Rp <span class="amount">{{ number_format($row->jumlah_diskon, 0, ',', '.') }}</span></span> </label>
              <input type="text" id="rupiah{{$row->id}}" name="jumlah_diskon" class="form-control" placeholder="Isikan Jumlah Membayar Siswa" value="{{$row->jumlah_diskon}}">
              <script>
                // Get the modals and images for each row
                var modal{{$row->id}} = document.getElementById("modal{{$row->id}}");
                var img{{$row->id}} = document.getElementById("myImg{{$row->id}}");
                img{{$row->id}}.onclick = function() {
                  modal{{$row->id}}.style.display = "block";
                  modalImg{{$row->id}}.src = this.src;
                }
                var spanmodal{{$row->id}} = document.getElementById("close{{$row->id}}");
                window.onclick = function(event) {
                  if (event.target === modal{{$row->id}}) {
                    modal{{$row->id}}.style.display = "none";
                  }
                }
                spanmodal{{$row->id}}.onclick = function() {
                modal{{$row->id}}.style.display = "none";
                }
                function formatRupiah(angka) {
                  var number_string = angka.toString();
                  var sisa = number_string.length % 3;
                  var rupiah = number_string.substr(0, sisa);
                  var ribuan = number_string.substr(sisa).match(/\d{3}/g);

                  if (ribuan) {
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                  }

                  return 'Rp ' + rupiah;
                }

                document.getElementById('rupiah{{$row->id}}').addEventListener('input', function () {
                    var inputValue = this.value;
                    var formattedValue = formatRupiah(inputValue);
                    var labeledValue = 'Jumlah Diskon: Rp ' + formattedValue; // Include the Rp. prefix here
                    document.getElementById('labelJumlah{{$row->id}}').innerText = labeledValue;
                });
                function unformatRupiah(formattedValue) {
                    // Remove 'Rp.' and thousand separators
                    return formattedValue.replace('Rp ', '').replace(/\./g, '');
                }
                // Format Rupiah Javascript
                var rupiah = document.getElementById('rupiah{{$row->id}}');
                var labelJumlah = document.getElementById('labelJumlah{{$row->id}}');
                rupiah.addEventListener('input', function() {
                    var inputValue = this.value;
                    var formattedValue = formatRupiah(inputValue);

                    // Update the label text
                    labelJumlah.innerText = 'Jumlah Diskon: Rp ' + formattedValue;
                });
                document.getElementById('edit').addEventListener('submit', function () {
                    var unformattedValue = this['jumlah_diskon'].value.replace('Rp  ', '').replace(/\./g, '');
                    this['jumlah_diskon'].value = unformattedValue;
                });

              </script>
              <div class="form-text"></div>
              <div class="form-text"></div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" >Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal fade" id="HapusDiskon{{$row->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Data Diskon </h5>
          <span type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">&times;</span>
        </div>
        <div class="modal-body">
            <form action="/admin/diskon/hapus/id={{ $row->id}}" method="GET" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="id"/>
              <div class="mb-3">
                <h3>Apakah anda Yakin ingin Menghapus data Ini?</h3>
                <br>
                <label class="form-label">Kode Pendaftaran : <?= $row->user->kode_pendaftaran?></label>
                <br>
                <label class="form-label">Nama Lengkap : <?= $row->user->nama_lengkap?></label>
            </div>
            <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Hapus</button>
      </div>
    </form>
  </div>
  @endforeach
</tbody>
</table>
</div>
<!-- /.card-body -->
</div>
<!-- /.card -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
<script>
  imgInp.onchange = evt => {
    const [file] = imgInp.files
    if (file) {
      blah.src = URL.createObjectURL(file)
    }
  }
  function countCheckedCheckboxes() {
    var checkboxes = document.querySelectorAll('input[name^="ids["]');
    var checkedCount = 0;

    checkboxes.forEach(function (checkbox) {
      if (checkbox.checked) {
        checkedCount++;
      }
    });

    return checkedCount;
  }

  function updateCheckedCount() {
    var checkedCount = countCheckedCheckboxes();
    document.getElementById('checkedCount').innerText = checkedCount;
  }

  function checkAll() {
    var checkAllCheckbox = document.querySelector('.check-all');
    var checkboxes = document.querySelectorAll('input[name^="ids["]');

    checkboxes.forEach(function (checkbox) {
      checkbox.checked = checkAllCheckbox.checked;
    });

    updateCheckedCount();
  }

  function submitmulti() {
    var checkedCount = countCheckedCheckboxes();

    if (checkedCount > 0) {
      // Get the form element
      var form = document.getElementById('myform');

      // Submit the form
      form.submit();
    } else {
      alert('Please select at least one checkbox to delete.');
    }
  }

  // Call updateCheckedCount initially and whenever checkboxes are changed
  updateCheckedCount();

  // Add event listener to update the count when checkboxes are changed
  var checkboxes = document.querySelectorAll('input[name^="ids["]');
  checkboxes.forEach(function (checkbox) {
    checkbox.addEventListener('change', updateCheckedCount);
  });

  // Add event listener to the check-all checkbox
  var checkAllCheckbox = document.querySelector('.check-all');
  checkAllCheckbox.addEventListener('change', checkAll);
  // Get the modal
  var modal = document.getElementById("myModal");

  // Get the image and insert it inside the modal - use its "alt" text as a caption
  var img = document.getElementById("myImg");
  var modalImg = document.getElementById("img01");
  img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
  }

  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];

  // When the user clicks on <span> (x), close the modal
    window.onclick = function(event) {
  if (event.target === modal) {
    modal.style.display = "none";
  }
    }
  span.onclick = function() {
    modal.style.display = "none";
  }
  
  var rupiah = document.getElementById('rupiah'); // Ensure rupiah is correctly referenced
    rupiah.addEventListener('keyup', function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        rupiah.value = formatRupiah(this.value, 'Rp. ');
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split = number_string.split(','),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] !== undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix === undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  }

    function unformatRupiah(rupiah) {
        // Remove non-numeric characters
        return rupiah.replace(/[^0-9]/g, '');
    }

    document.getElementById('edit').addEventListener('submit', function() {
        // Update the jumlah_diskon input value directly
        rupiah.value = unformatRupiah(rupiah.value);
    });

</script>
  @endsection