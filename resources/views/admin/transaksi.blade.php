@extends('admin.layout.main')
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Transaksi</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  @if(session()->has('success'))
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script>
      Swal.fire({
          icon: 'success',
          title: 'Success!',
          text: '{{ session("success") }}',
      });
  </script>
  @endif
  <!-- /.content-header -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <button type="button" onclick="buttonmulti()" class="btn btn-danger">
              Hapus
            </button>
            </div>
          <div class="modal fade" id="Hapusmulti" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Hapus Data Transaksi </h5>
                  <span type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">&times;</span>
                </div>
                <div class="modal-body">
                  <h3>Apakah anda Yakin ingin Menghapus </h3><h3 id="checkedCount">0</h3><h3> baris Ini?</h3>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" onclick="submitmulti()">Hapus</button>
                </div>
              </div>
            </div>
          </div>
          <form action="/admin/transaksi/multidel" id="myform" method="POST">
            @csrf
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
      <thead>
          <tr align="left">
            <th scope="col"><input type="checkbox" class="check-all"></th>
              <th scope="col">No</th>
              <th scope="col">Kode Pendaftaran</th>
              <th scope="col">Nama Lengkap</th>
              <th scope="col">Bukti Transfer</th>
              <th scope="col">Bukti Diskon</th>
              <th scope="col">Jenis Diskon</th>
              <th scope="col">Jumlah Diskon</th>
              <th scope="col">Jumlah Membayar</th>
              <th scope="col">Total Membayar</th>
              <th scope="col">Jumlah Biaya</th>
              <th scope="col">Jumlah Sisa</th>
              <th scope="col">Keterangan</th>
              <th scope="col">Aksi</th>
          </tr>
      </thead>
      <tbody>
        <?php $no= 1; ?>
          @foreach ($data as $row)
          <tr>
            <td>
              <input type="checkbox" name="ids[{{ $row->id }}]" value="{{ $row->id }}">
            </td>
          </form>
              <th>{{ $no++}}</th>
              <td>{{  $row->user->kode_pendaftaran }}</td>
              <td>{{$row->user->nama_lengkap}}</td>

              <td><img id="myImgTransfer{{$row->id}}" src="{{ asset('bukti_transfer/'.$row->bukti_transfer) }}" alt="" width="70px" height="80px"></td>
              <td> @if($row->diskon->bukti_diskon == "Tidak Memiliki Potongan")
                {{ $row->diskon->bukti_diskon }}
                @else
                <img id="myImgDiskon{{$row->id_diskon}}" src="{{ asset('bukti_diskon/'.$row->diskon->bukti_diskon) }}" alt="" width="70px" height="80px">
                @endif
              </td>

              
              <td>{{$row->diskon->jenis_diskon}}</td>
              <td><span class="rupiah-format" data-amount="{{ $row->diskon->jumlah_membayar }}">Rp. <span class="amount">{{ number_format($row->diskon->jumlah_membayar, 0, ',', '.') }}</span></span></td>
              <td><span class="rupiah-format" data-amount="{{ $row->jumlah_membayar }}">Rp. <span class="amount">{{ number_format($row->jumlah_membayar, 0, ',', '.') }}</span></span></td>
              <td><span class="rupiah-format" data-amount="{{ $row->total_membayar }}">Rp. <span class="amount">{{ number_format($row->total_membayar, 0, ',', '.') }}</span></span></td>
              <td><span class="rupiah-format" data-amount="{{ $row->jumlah_biaya }}">Rp. <span class="amount">{{ number_format($row->jumlah_biaya, 0, ',', '.') }}</span></span></td>
              <td><span class="rupiah-format" data-amount="{{ $row->jumlah_sisa }}">Rp. <span class="amount">{{ number_format($row->jumlah_sisa, 0, ',', '.') }}</span></span></td>
              <td>{{$row->keterangan}}</td>
              <td>
                <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#EditTransaksi{{$row->id}}">
                    Edit
                </button>
                <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#HapusTransaksi{{$row->id}}">
                  Hapus
                </button>
              </td>
          </tr>
           <!-- Modal for the first image -->
          <div id="modalTransfer{{$row->id}}" class="modal-preview">
            
                <span class="close" id="closeTransfer{{$row->id}}" style="color: white">&times;</span>
                <img class="modal-image" id="modalImgTransfer{{$row->id}}" width="150px" height="300px">
            
          </div>

          <!-- Modal for the second image -->
          <div id="modalDiskon{{$row->id_diskon}}" class="modal-preview">
            
                <span class="close" id="closeDiskon{{$row->id_diskon}}"style="color: white">&times;</span>
                <img class="modal-image" id="modalImgDiskon{{$row->id_diskon}}" width="150px" height="300px">
            
          </div>
          <script>
          // JavaScript for Transfer Modal
            var modalTransfer{{$row->id}} = document.getElementById("modalTransfer{{$row->id}}");
            var imgTransfer{{$row->id}} = document.getElementById("myImgTransfer{{$row->id}}");
            
            imgTransfer{{$row->id}}.onclick = function() {
              modalTransfer{{$row->id}}.style.display = "block";
              modalImgTransfer{{$row->id}}.src = this.src;
            }
            
            var spanmodalTransfer{{$row->id}} = document.getElementById("closeTransfer{{$row->id}}");
            spanmodalTransfer{{$row->id}}.onclick = function() {
              modalTransfer{{$row->id}}.style.display = "none";
            }
            
            // JavaScript for Discount Modal
            var modalDiskon{{$row->id_diskon}} = document.getElementById("modalDiskon{{$row->id_diskon}}");
            var imgDiskon{{$row->id_diskon}} = document.getElementById("myImgDiskon{{$row->id_diskon}}");
            
            imgDiskon{{$row->id_diskon}}.onclick = function() {
              modalDiskon{{$row->id_diskon}}.style.display = "block";
              modalImgDiskon{{$row->id_diskon}}.src = this.src;
            }
            
            var spanmodalDiskon{{$row->id_diskon}} = document.getElementById("closeDiskon{{$row->id_diskon}}");
            spanmodalDiskon{{$row->id_diskon}}.onclick = function() {
              modalDiskon{{$row->id_diskon}}.style.display = "none";
            }
            
            // Close modals on window click
            window.onclick = function(event) {
              if (event.target === modalTransfer{{$row->id}}) {
                modalTransfer{{$row->id}}.style.display = "none";
              }
            
              if (event.target === modalDiskon{{$row->id_diskon}}) {
                modalDiskon{{$row->id_diskon}}.style.display = "none";
              }
            }
            </script>

  <div class="modal fade" id="EditTransaksi{{$row->id}}" tabindex="-1" aria-labelledby="EditTransaksi{{$row->id}}" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Data Transaksi </h5>
          <span type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">&times;</span>
        </div>
        <div class="modal-body">
          <!-- Nav tabs -->
          <form action="/admin/transaksi/update/id={{$row->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            <!-- Tab buttons -->
            <div class="nav nav-tabs" id="nav-tab{{$row->id}}" role="tablist">
              <a class="nav-item nav-link active" id="nav-jumlah-tab{{$row->id}}" data-toggle="tab" href="#nav-jumlah{{$row->id}}" role="tab" aria-controls="nav-jumlah{{$row->id}}" aria-selected="true">Jumlah Membayar</a>
              <a class="nav-item nav-link" id="nav-total-tab{{$row->id}}" data-toggle="tab" href="#nav-total{{$row->id}}" role="tab" aria-controls="nav-total{{$row->id}}" aria-selected="false">Total Membayar</a>
            </div>

            <!-- Tab panes -->
            <div class="tab-content" id="nav-tabContent{{$row->id}}">
              <div class="tab-pane fade show active" id="nav-jumlah{{$row->id}}" role="tabpanel" aria-labelledby="nav-jumlah-tab{{$row->id}}">
                <label class="form-label" id="labeljumlahMembayar{{$row->id}}">Jumlah Membayar: <span class="rupiah-format" data-amount="{{ $row->jumlah_membayar }}">Rp <span class="amount">{{ number_format($row->jumlah_membayar, 0, ',', '.') }}</span></span></label>
                <input type="text" name="jumlah_membayar" id="rupiahMembayar{{$row->id}}" class="form-control" placeholder="Isikan Jumlah Transfer Pengguna" value="{{ $row->jumlah_membayar }}">
              </div>
              <div class="tab-pane fade" id="nav-total{{$row->id}}" role="tabpanel" aria-labelledby="nav-total-tab{{$row->id}}">
                <label class="form-label" id="labelTotalMembayar{{$row->id}}">Total Membayar: <span class="rupiah-format" data-amount="{{ $row->total_membayar }}">Rp <span class="amount">{{ number_format($row->total_membayar, 0, ',', '.') }}</span></span></label>
                <input type="text" name="total_membayar" id="rupiahTotalMembayar{{$row->id}}" class="form-control" placeholder="Isikan Jumlah Transfer Pengguna" value="{{ $row->total_membayar }}">
              </div>
            </div>
          <script>
        const rupiahMembayar{{$row->id}} = document.getElementById('rupiahMembayar{{$row->id}}');
const labeljumlahMembayar{{$row->id}} = document.getElementById('labeljumlahMembayar{{$row->id}}');

rupiahMembayar{{$row->id}}.addEventListener('input', function() {
  const value = this.value.replace(/\D/g, ''); // Remove non-numeric characters
  const formattedValue = formatRupiah(value);
  labeljumlahMembayar{{$row->id}}.textContent = 'Jumlah Membayar: ' + formattedValue;
});

const rupiahTotalMembayar{{$row->id}} = document.getElementById('rupiahTotalMembayar{{$row->id}}');
const labelTotalMembayar{{$row->id}} = document.getElementById('labelTotalMembayar{{$row->id}}');

rupiahTotalMembayar{{$row->id}}.addEventListener('input', function() {
  const value = this.value.replace(/\D/g, ''); // Remove non-numeric characters
  const formattedValue = formatRupiah(value);
  labelTotalMembayar{{$row->id}}.textContent = 'Total Membayar: ' + formattedValue;
});

    function formatRupiah(value) {
      const formatter = new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR',
        minimumFractionDigits: 0
      });
      return formatter.format(value);
    }
            // Get the tabs
    var tabs = document.getElementsByClassName("tab");

// Get the tab buttons
var tabButtons = document.getElementsByClassName("tab-button");

// When the page loads, hide the navtotalMembayar{{$row->id}} tab
document.getElementById('navtotalMembayar{{$row->id}}').style.display = "none";

// Function to open a specific tab
function openTab(evt, tabName) {
  
    // Hide all tabs
    for (var i = 0; i < tabs.length; i++) {
        tabs[i].style.display = "none";
    }

    // Deactivate all tab buttons
    for (var i = 0; i < tabButtons.length; i++) {
        tabButtons[i].classList.remove("active");
    }

    // Show the selected tab
    document.getElementById(tabName).style.display = "block";

    // Activate the selected tab button
    evt.currentTarget.classList.add("active");

}
          </script>
          <!-- Button to open child modal -->
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" onclick="window.location.reload();">Close</button>

          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
          </form>
        </div>
    </div>
      </div>
      
  <div class="modal fade" id="HapusTransaksi{{$row->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Data Transaksi </h5>
          <span type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">&times;</span>
        </div>
        <div class="modal-body">
            <form action="/admin/transaksi/hapus/id={{ $row->id}}" method="GET" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="id"/>
              <div class="mb-3">
                <h3>Apakah anda Yakin ingin Menghapus data Ini?</h3>
                <br>
                <label class="form-label">Kode Pendaftaran : <?= $row->user->kode_pendaftaran?></label>
                <br>
                <label class="form-label">Nama Lengkap : <?= $row->user->nama_lengkap?></label>
            </div>
            <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Hapus</button>
      </div>
    </form>
  </div>
</div>
    </div>
  </div>
  @endforeach
</tbody>
</table>
</div>
<!-- /.card-body -->
</div>
<!-- /.card -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
  <!-- Modal -->
  <script>
    
  function countCheckedCheckboxes() {
    var checkboxes = document.querySelectorAll('input[name^="ids["]');
    var checkedCount = 0;

    checkboxes.forEach(function (checkbox) {
      if (checkbox.checked) {
        checkedCount++;
      }
    });

    return checkedCount;
  }

  function updateCheckedCount() {
    var checkedCount = countCheckedCheckboxes();
    document.getElementById('checkedCount').innerText = checkedCount;
  }

  function checkAll() {
    var checkAllCheckbox = document.querySelector('.check-all');
    var checkboxes = document.querySelectorAll('input[name^="ids["]');

    checkboxes.forEach(function (checkbox) {
      checkbox.checked = checkAllCheckbox.checked;
    });

    updateCheckedCount();
  }
function buttonmulti(){
  var checkedCount = countCheckedCheckboxes();
  if (checkedCount > 0) {
      // Get the modal element
      var modal = new bootstrap.Modal(document.getElementById('Hapusmulti'));
      modal.show();
    } else {
      alert('Tolong Seleksi Setidaknya 1 .');
    }
}
  function submitmulti() {
    var checkedCount = countCheckedCheckboxes();

    if (checkedCount > 0) {
      // Get the form element
      var form = document.getElementById('myform');

      // Submit the form
      form.submit();
    } else {
      alert('Tolong Seleksi Setidaknya 1 .');
    }
  }

  // Call updateCheckedCount initially and whenever checkboxes are changed
  updateCheckedCount();

  // Add event listener to update the count when checkboxes are changed
  var checkboxes = document.querySelectorAll('input[name^="ids["]');
  checkboxes.forEach(function (checkbox) {
    checkbox.addEventListener('change', updateCheckedCount);
  });

  // Add event listener to the check-all checkbox
  var checkAllCheckbox = document.querySelector('.check-all');
  checkAllCheckbox.addEventListener('change', checkAll);
</script>
  @endsection