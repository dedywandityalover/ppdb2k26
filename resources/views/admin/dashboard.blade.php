@extends('admin.layout.main')
@section('content')
<?php $a = \App\Models\User::where('is_admin', 0)
                            ->orWhereNull('is_admin')->count();?> 
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row ">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content md-10">
      <div class="row">
        <div class="col-12">
          <div class="card p-4">
            <div class="d-flex justify-content-center" style="margin-right: 29%; padding" > <!-- Add this container for flex display -->
                <div class="card border-left-primary shadow h-100 py-2" style="width:800px;" >
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class=" font-weight-bold text-primary text-uppercase mb-1" style="font-size: 25px">
                            Total Siswa Registrasi</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$a}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-graduation-cap fa-2x text-gray-300"></i>
                    </div>
                    </div>
                </div>
                </div>
                <div class="col-xl-6 " style="padding-left:30px"">
                    <div class="card border-left-primary shadow py-2 " style="width: 750px; height:350px;">
                    <div class="card-body">
                            {!! $chart->container() !!}
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <script src="{{ $chart->cdn() }}"></script>
            {{ $chart->script() }}
            
          </div>
    
    </div>
  </section>
        </div>
      </div>
    </section>
</div>
@endsection
