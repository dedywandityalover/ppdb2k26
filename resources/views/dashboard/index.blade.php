@extends('dashboard.halaman.main')
 <!-- Content Start -->
 <div class="content">
    <!-- Navbar Start -->
    <nav class="navbar navbar-expand bg-light navbar-light sticky-top px-4 py-0">
        <a href="index.html" class="navbar-brand d-flex d-lg-none me-4">
                <img src="assets/img/Logo-sekolah.png" height="50" width="50">
        </a>
        <a href="#" class="sidebar-toggler d-flex ">
            <i class="fa fa-bars"></i>
        </a>
    </nav>
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center p-4 pt-3 pb-2 mb-3 border-bottom">
        <h4>Selamat Datang, {{ auth()->user()->nama_lengkap }}</h4>
      </div>
      <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-5">Alur Pendaftaran PPDB</h4>
        
                        <div class="hori-timeline" dir="ltr">
                            <ul class="list-inline events d-flex justify-content-center">
                                <li class="list-inline-item event-list">
                                    <div class="px-4">
                                        <div class="event-date bg-soft-primary text-primary">1</div>
                                        <h5 class="font-size-16">▼</h5>
                                        <p class="text-muted">Pengisian data diri secara lengkap<br><br><br><br></p>
                                    </div>
                                </li>
                                <li class="list-inline-item event-list">
                                    <div class="px-4">
                                        <div class="event-date bg-soft-success text-success">2</div>
                                        <h5 class="font-size-16">▼</h5>
                                        <p class="text-muted">Pembayaran dengan menyertakan bukti transfer <br><br><br><br></p>
                                    </div>
                                </li>
                                <li class="list-inline-item event-list">
                                    <div class="px-4">
                                        <div class="event-date bg-soft-danger text-danger">3</div>
                                        <h5 class="font-size-16">▼</h5>
                                        <p class="text-muted">Gelombang Early Bird berakhir pada tanggal 14 Februari 2024 <br><br><br><br></p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- end card -->
            </div>
        </div>
        </div>


      <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button collapsed bg-white text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                               Biaya Gelombang Early Bird
                            </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Necessitatibus, facere?
                            </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingTwo">
                            <button class="accordion-button collapsed bg-white text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              Biaya Gelombang 1
                            </button>
                            </h2>
                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                             Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel, quibusdam!
                            </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button collapsed bg-white text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Biaya Gelombang 2
                            </button>
                            </h2>
                            <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                               Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima quaerat quas officia mollitia neque quod doloribus, dolorum accusantium iusto vel maxime, dicta omnis officiis a consectetur illo natus modi. Iusto.
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <!-- end card -->
            </div>
        </div>
        </div>

      <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                        <div class="col-sm-6">
                            <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Siswa</h5>
                                <p class="card-text">Isilah data siswa dengan sebenar benarnya</p>
                                <a href="/siswa" class="btn btn-primary">Formulir siswa</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Orang Tua</h5>
                                <p class="card-text">Isilah data orang tua dengan sebenar benarnya</p>
                                <a href="/ortu" class="btn btn-primary">Formulir Orang Tua</a>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <!-- end card -->
            </div>
        </div>
        </div>

        </div>
        

        
    