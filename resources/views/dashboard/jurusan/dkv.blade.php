@extends('dashboard.halaman.main')
 <!-- Content Start -->
 <div class="content">
    <!-- Navbar Start -->
    <nav class="navbar navbar-expand bg-light navbar-light sticky-top px-4 py-0">
        <a href="index.html" class="navbar-brand d-flex d-lg-none me-4">
                <img src="assets/img/Logo-sekolah.png" height="50" width="50">
        </a>
        <a href="#" class="sidebar-toggler d-flex ">
            <i class="fa fa-bars"></i>
        </a>
    </nav>
    <div class="container-fluid">
    <div class="card mb-3">
        <img src="..." class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">Multimedia</h5>
          <p class="card-text">Multimedia merupakan bidang yang berkaitan dengan penggunaan berbagai media untuk menyampaikan informasi ke publik, termasuk media digital dan media cetak. Jurusan ini umumnya sangat luas dan berkaitan dengan banyak hal, tergantung apa yang kamu minati dan dalami. Materi yang dipelajari pun sangat beragam, mulai dari televisi, ilmu film, jurnalisme, produksi video, media interaktif, hingga animasi komputer. Selain teori, tentunya bidang ini akan memberikan praktik seperti membuat website, membuat dan mengedit video, desain grafis, membuat animasi, dan membuat konten lainnya untuk berbagai jenis media.</p>
          <br>
          <h5 class="card-title">Prospek Kerja Jurusan Multimedia</h5>
          <p class="card-text">1.Web Designer <br>
            2.Graphic Designer <br>
            3.Interactive Media Designer <br>
            4.Game Modeler/Designer
        </div>
      </div>
    </div>
</div>