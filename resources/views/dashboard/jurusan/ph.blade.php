@extends('dashboard.halaman.main')
 <!-- Content Start -->
 <div class="content">
    <!-- Navbar Start -->
    <nav class="navbar navbar-expand bg-light navbar-light sticky-top px-4 py-0">
        <a href="index.html" class="navbar-brand d-flex d-lg-none me-4">
                <img src="assets/img/Logo-sekolah.png" height="50" width="50">
        </a>
        <a href="#" class="sidebar-toggler d-flex ">
            <i class="fa fa-bars"></i>
        </a>
    </nav>
    <div class="container-fluid">
    <div class="card mb-3">
        <img src="..." class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">Perhotelan</h5>
          <p class="card-text">Perkembangan industri pariwisata yang semakin pesat tak lepas dari tumbuhnya bisnis perhotelan. Hal ini menjanjikan masa depan yang cerah bagi para lulusan Jurusan Perhotelan dengan peluang kerja yang terbuka lebar. Akomodasi Perhotelan atau sering disingkat AP adalah program keahlian yang menerapkan dan mengembangkan pengetahuan dan keterampilan di bidang ilmu perhotelan atau hospitality. Jurusan ini merupakan kesatuan rencana belajar yang menerapkan dan mengembangkan pengetahuan, keterampilan dan ilmu perhotelan khususnya dalam kegiatan operasional jasa penginapan atau akomodasi serta food and beverage service.</p>
          <br>
          <h5 class="card-title">Profil Lulusan Kompetensi keahlian Perhotelan</h5>
          <p class="card-text">Setiap peserta didik yang menyelesaikan pendidikan dan pelatihan bidang Akomodasi Perhotelan diharapkan telah memiliki pengetahuan, keterampilan dan sikap yang memadai sehingga kompeten untuk : <br>
            1. Melaksanakan pekerjaan di lingkup Front Office sebagai Receptionist, Reservation Staff, Telephone Operator, dan Porter; <br>
            2. Melaksanakan pekerjaan di lingkup Housekeeping sebagai Public Area Attendant, Room Attendant, Order Taker, Linen and Uniform Attendant serta Laundry Attendant <br>
            3. Melaksanakan pekerjaan di lingkup Food and beverage service sebagai waiter/waitress. <br>
            <h5 class="card-title">Pekerjaan dibidang Perhotelan</h5>
            <p class="card-text">1. Wirausaha <br>
                2. Room Attendant <br>
                3. Bellboy / Porter <br>
                4. Receptionist <br>
                5. Telephone Operator <br>
                6. Reservation Staff <br>
                7. Public Area Attendant <br>
                8. Laundry Attendant <br>
                9. Usaha Pelayanan Cleaning Service <br>
                10. Usaha Pelayanan Laundry <br>
                11. Waiter/Waitress</p>
        </div>
      </div>
    </div>
</div>