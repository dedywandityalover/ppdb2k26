@extends('dashboard.halaman.main')
 <!-- Content Start -->
 <div class="content">
    <!-- Navbar Start -->
    <nav class="navbar navbar-expand bg-light navbar-light sticky-top px-4 py-0">
        <a href="index.html" class="navbar-brand d-flex d-lg-none me-4">
                <img src="assets/img/Logo-sekolah.png" height="50" width="50">
        </a>
        <a href="#" class="sidebar-toggler d-flex ">
            <i class="fa fa-bars"></i>
        </a>
    </nav>
    <div class="container-fluid">
    <div class="card mb-3">
        <img src="..." class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">Rekayasa Perangkat Lunak</h5>
          <p class="card-text">RPL adalah singkatan dari Rekayasa Perangkat Lunak dan merupakan sebuah jurusan yang ada di Sekolah Menengah Kejuruan (SMK). RPL adalah sebuah jurusan yang mempelajari dan mendalami semua cara-cara pengembangan perangkat lunak termasuk pembuatan, pemeliharaan, manajemen organisasi pengembangan perangkat lunak dan manajemen kualitas. Bukan hanya itu, RPL juga berkaitan dengan software komputer mulai dari pembuatan website, aplikasi, game dan semua yang berkaitan dengan pemrograman dengan menguasai bahasa pemrograman tersebut. Intinya RPL tidak akan jauh-jauh dari tiga hal yaitu Coding, Desain dan Algoritma yang akan menjadi kunci keberhasilan rekayasa perangkat lunak tersebut.</p>
          <br>
          <h5 class="card-title">Keunggulan Jurusan RPL</h5>
          <p class="card-text">1.Mampu bekerja di berbagai bidang karena sudah dibekali dengan berbagai ilmu dan pengetahuan. <br>
            2.Dalam melakukan kerja lapangan akan lebih mudah karena saat pembelajaran sudah sering melakukan kerja praktek. <br>
            3.Pekerjaan nya yang relatif mudah dan santai, dapat dikerjakan dimanapun dan kapanpun menggunakan koneksi tentunya. <br>
            4.Lebih terdepan dari jurusan lainnya dan orang awam diluar sana karena jurusan RPL lebih mengerti dan mendalami berbagai teknologi. <br>
            5.Mampu memasang sebuah PC. <br>
            6.Mengerti akan fungsi dari komponen komputer.
        </div>
      </div>
    </div>
</div>
        

        
    
