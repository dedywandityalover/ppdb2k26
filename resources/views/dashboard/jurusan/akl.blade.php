@extends('dashboard.halaman.main')
 <!-- Content Start -->
 <div class="content">
    <!-- Navbar Start -->
    <nav class="navbar navbar-expand bg-light navbar-light sticky-top px-4 py-0">
        <a href="index.html" class="navbar-brand d-flex d-lg-none me-4">
                <img src="assets/img/Logo-sekolah.png" height="50" width="50">
        </a>
        <a href="#" class="sidebar-toggler d-flex ">
            <i class="fa fa-bars"></i>
        </a>
    </nav>
    <div class="container-fluid">
    <div class="card mb-3">
        <img src="..." class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">Akuntansi dan Keuangan Lembaga</h5>
          <p class="card-text">Akuntansi adalah segala proses yang berhubungan dengan transaksi keuangan mulai dari pencatatan, mengklasifikasi jenis transaksi, meringkasnya, mengolah dan menjadikan sebuah data, tujuannya adalah untuk menjadikan sebuah laporan keuangan yang akurat. Kompetensi Keahlian Akuntansi dan Keuangan Lembaga bertujuan untuk menyiapkan siswa/tamatan yang berkompeten dibidang Akuntansi dalam proses pengolahan data keuangan suatu perusahaan dan lembaga untuk menghasilkan laporan keuangan bagi pihak pihak yang berkepentingan atau pihak-pihak yang membutuhkan sekaligus mampu menerapkan system perpajakan di Indonesia.</p>
          <br>
          <h5 class="card-title">Pekerjaan di Bidang Akuntansi</h5>
          <p class="card-text">1.Akuntan Perusahaan <br>
            2.Internal Auditor <br>
            3.Pegawai Lembaga Keuangan Pemerintahan <br>
            4.Teller <br>
            5.Accounting <br>
            6.Finance <br>
            7.Admin Keuangan Perusahaan <br>
            8.Perpajakan <br>
            9.Administrasi Perkantoran
        </div>
      </div>
    </div>
</div>