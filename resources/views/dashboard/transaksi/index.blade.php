@extends('dashboard.halaman.main')
<div class="content">
    <nav class="navbar navbar-expand bg-light navbar-light sticky-top px-4 py-0">
        <a href="index.html" class="navbar-brand d-flex d-lg-none me-4">
                <img src="assets/img/Logo-sekolah.png" height="50" width="50">
        </a>
        <a href="#" class="sidebar-toggler d-flex ">
            <i class="fa fa-bars"></i>
        </a>
    </nav>

    @if(session()->has('success'))
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script>
      Swal.fire({
          icon: 'success',
          title: 'Berhasil!',
          text: '{{ session("success") }}',
      });
  </script>
  @endif


  @if(session()->has('error'))
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script>
      Swal.fire({
          icon: 'error',
          title: 'Gagal!',
          text: '{{ session("error") }}',
      });
  </script>
  @endif

      @php
          $siswa = App\Models\data_peserta::where('id_user', auth()->user()->id)->first();
          $transaksi = App\Models\transaksi::where('id_user', auth()->user()->id)->exists();
      @endphp
  
      @if (!$siswa)
      <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
      <script>
          Swal.fire({
              icon: 'warning',
              title: 'Perhatian!',
              text: 'Harap isi formulir siswa terlebih dahulu sebelum mengisi formulir Transaksi.',
            }).then(() => {
            // Redirect ke halaman siswa
            window.location.href = '/siswa';
          });
      </script>
      
      @elseif($transaksi)

      <div class="login-wrap p-4 p-md-5">
        <div class="d-flex">
            <div class="w-100">
                <h3 class="mb-4">Transaksi</h3>
            </div>
        </div>
              <form action="/transaksi/update" method="POST" class="signin-form"  enctype="multipart/form-data">
                  @csrf
                  <div class="form-group mb-3">
                    <input type="hidden" class="form-control @error('id_user')is-invalid @enderror" name="id_user" value="{{ auth()->user()->id }}"  required value="{{ old('id_user') }}">
                    @error('id_user')<div class="invalid-feedback">{{ $message }}</div>@enderror
                  </div>
          
                  <div class="form-group mb-3">
                    <input type="hidden" class="form-control @error('id_peserta')is-invalid @enderror" name="id_peserta" value="{{ auth()->user()->peserta->id }}"  required value="{{ old('id_peserta') }}">
                    @error('id_peserta')<div class="invalid-feedback">{{ $message }}</div>@enderror
                  </div>
          
                  <div class="form-group mb-3">
                    <input type="hidden" class="form-control @error('id_diskon')is-invalid @enderror" name="id_diskon" value="{{ auth()->user()->diskon->id }}"  required value="{{ old('id_diskon') }}">
                    @error('id_diskon')<div class="invalid-feedback">{{ $message }}</div>@enderror
                  </div>

        <div class="form-group mb-2">
        <h6>Jenis Potongan</h6>
        <label class="form-control bg-light text-dark">{{ auth()->user()->diskon->jenis_diskon }}</label>
        </div>

      <div class="form-group mb-2">
      <h6>*) Bukti Foto Surat Keterangan dari domisili Kuta Selatan <br>
        *) Bukti Foto Voucher STC <br>
        *) Bukti Foto Piagam Prestasi Juara Umum selama 3 tahun dijadikan satu gambar <br>
        *) Bukti Foto Piagam Prestasi Juara Kelas selama 3 tahun dijadikan satu gambar</h6>

        @if( auth()->user()->diskon->bukti_diskon  == "Tidak Memiliki Potongan")
        <label class="form-control bg-light text-dark">{{ auth()->user()->diskon->bukti_diskon }}</label>
        @else
        <img src="{{ asset('bukti_diskon/'.auth()->user()->diskon->bukti_diskon) }}" alt="" width="100px" height="100px">
        </div>
        @endif

      <div class="form-group mb-3 mt-4">
        <h6>Upload Bukti Transfer Tambahan</h6>
        <img src="{{ asset('bukti_transfer/'.auth()->user()->transaksi->latest()->value('bukti_transfer')) }}" alt="" width="100px" height="100px">
        <div class="form-group mb-3 mt-4">
          <input type="file" class="form-control" name="bukti_transfer">
      
          @error('bukti_transfer')
              <div class="alert alert-danger mt-2">
                  {{ $message }}
              </div>
          @enderror
      </div>
      </div>
 
  
      <div class="form-group mb-3">
        <h6>Gelombang Pendaftaran</h6>
        <label class="form-control bg-warning text-white" value="{{ auth()->user()->gelombang_pendaftaran }}"> {{ auth()->user()->gelombang_pendaftaran }} </label>
      </div>

      <div class="form-group mb-2">
        <h6>Jumlah Diskon</h6>
        <label class="form-control bg-light text-dark">{{ formatMoney(auth()->user()->diskon->jumlah_diskon) }}</label>
      </div>

      <div class="form-group mb-2">
        <h6>Jumlah Membayar</h6>
        <label class="form-control bg-light text-dark"> Rp {{ number_format(auth()->user()->transaksi->latest()->value('jumlah_membayar'), 0, ',', '.') }}</label>
        <input type="hidden" class="form-control bg-light text-dark" name="jumlah_membayar" value="0"></input>
        </div>

      <div class="form-group mb-2">
        <h6>Total Membayar</h6>
        <label class="form-control bg-light text-dark"> Rp {{ number_format(auth()->user()->transaksi->latest()->value('total_membayar'), 0, ',', '.') }}</label>
        <input type="hidden" class="form-control bg-light text-dark" name="total_membayar" value="{{ auth()->user()->transaksi->latest()->value('total_membayar') }}" ></input>
        </div>

      <div class="form-group mb-2">
        <h6>Jumlah Biaya</h6>
        <label class="form-control bg-light text-dark"> Rp {{ number_format(auth()->user()->transaksi->latest()->value('jumlah_biaya'), 0, ',', '.') }}</label>
        <input type="hidden" class="form-control bg-light text-dark" name="jumlah_biaya" value="{{ auth()->user()->transaksi->latest()->value('jumlah_biaya') }}"></input>
        </div>

      <div class="form-group mb-2">
        <h6>Jumlah Sisa</h6>
        <label class="form-control bg-light text-dark"> Rp {{ number_format(auth()->user()->transaksi->latest()->value('jumlah_sisa'), 0, ',', '.') }}</label>
        <input type="hidden" class="form-control bg-light text-dark" name="jumlah_sisa" value="{{ auth()->user()->transaksi->latest()->value('jumlah_sisa') }}"></input>
        </div>

      <div class="form-group mb-2">
        <h6>Keterangan</h6>
        <label class="form-control bg-light text-dark">{{ auth()->user()->transaksi->latest()->value('keterangan') }}</label>
        <input type="hidden" class="form-control bg-light text-dark" name="keterangan" value="Akan Divalidasi Oleh Admin MAX 24 Jam"></input>
        </div>
        <div class="form-group">
          <button type="submit" class="form-control btn btn-primary rounded submit px-3">Kirim</button>
      </div>
    </form>
      </div>

      @else
   
    {{-- formulir  --}}
    <div class="login-wrap p-4 p-md-5">
        <div class="d-flex">
            <div class="w-100">
                <h3 class="mb-4">Transaksi</h3>
            </div>
        </div>
              <form action="/transaksi" method="POST" class="signin-form"  enctype="multipart/form-data">
                  @csrf
      <div class="form-group mb-21">

        <div class="form-group mb-3">
          <input type="hidden" class="form-control @error('id_user')is-invalid @enderror" name="id_user" value="{{ auth()->user()->id }}"  required value="{{ old('id_user') }}">
          @error('id_user')<div class="invalid-feedback">{{ $message }}</div>@enderror
        </div>

        <div class="form-group mb-3">
          <input type="hidden" class="form-control @error('id_peserta')is-invalid @enderror" name="id_peserta" value="{{ auth()->user()->peserta->id }}"  required value="{{ old('id_peserta') }}">
          @error('id_peserta')<div class="invalid-feedback">{{ $message }}</div>@enderror
        </div>

        <div class="form-group mb-2">
          <h6>Jenis Potongan</h6>
          <select class="form-control @error('jenis_diskon') is-invalid @enderror" name="jenis_diskon" onchange="checkLainnya(this)" required>
              <option value="">Jenis Potongan</option>
              <option value="Tidak Punya">Tidak Punya</option>
              <option value="STC">STC (Rp. 800.000)</option>
              <option value="Juara Umum">Juara Umum Dari Kelas 1 sampai Kelas 3 dan Peringkat 1 sampai 3 (Rp. 1.200.000)</option>
              <option value="Juara Kelas">Juara Kelas Dari Kelas 1 sampai Kelas 3 dan Peringkat 1 sampai 3 (Rp. 800.000)</option>
              <option value="Daerah Kuta Selatan">Daerah Kuta Selatan (Rp. 1000.000)</option>
              <option value="Lainnya">Lainnya</option>
          </select>
        @error('jenis_diskon')<div class="invalid-feedback">{{ $message }}</div>@enderror
      </div>

      <div class="form-group mb-2" id="lainnyaInput" style="display: none;">
        <label for="lainnya">Jenis Potongan Lainnya</label>
        <input type="text" class="form-control" name="lainnya" id="lainnya">
    </div>

      <div class="form-group mb-2">
      <h6>*) Bukti Foto Surat Keterangan dari domisili Kuta Selatan <br>
        *) Bukti Foto Voucher STC <br>
        *) Bukti Foto Piagam Prestasi Juara Umum selama 3 tahun dijadikan satu gambar <br>
        *) Bukti Foto Piagam Prestasi Juara Kelas selama 3 tahun dijadikan satu gambar</h6>
        <div class="form-group mb-3 mt-4">
          <input type="file" class="form-control" name="bukti_diskon">
      
          @error('bukti_diskon')
              <div class="alert alert-danger mt-2">
                  {{ $message }}
              </div>
          @enderror
      </div>
      </div>

      <div class="form-group mb-3 mt-4">
        <h6>Upload Bukti Transfer</h6>
        <input type="file" class="form-control" name="bukti_transfer">
    
        @error('bukti_transfer')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>

      @php
      $gelombang = auth()->user()->gelombang_pendaftaran;
      $biaya_gelombang = 0;
      if($gelombang == "Early Bird"){
          $biaya_gelombang = 10250000;
      }
      elseif($gelombang == "Gelombang 1"){
          $biaya_gelombang = 11750000;
      }
      elseif($gelombang == "Gelombang 2"){
          $biaya_gelombang = 12750000;
      }
      @endphp
      
      <div class="form-group mb-3">
        <h6>Gelombang Pendaftaran</h6>
        <label class="form-control bg-warning text-white" value="{{ auth()->user()->gelombang_pendaftaran }}"> {{ auth()->user()->gelombang_pendaftaran }} </label>
      </div>
      <input type="hidden" name="jumlah_biaya" value="{{$biaya_gelombang}}">

      <div class="form-group">
          <button type="submit" class="form-control btn btn-primary rounded submit px-3">Kirim</button>
      </div>
    </form>
  </div>


  <script>
    function checkLainnya(selectElement) {
        var lainnyaInput = document.getElementById('lainnyaInput');
        var lainnyaInputField = document.getElementById('lainnya');

        if (selectElement.value === 'Lainnya') {
            lainnyaInput.style.display = 'block';
            lainnyaInputField.required = true;
        } else {
            lainnyaInput.style.display = 'none';
            lainnyaInputField.required = false;
        }
    }
</script>
  @endif
        
</div>




