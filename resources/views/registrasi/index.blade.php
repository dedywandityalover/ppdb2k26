<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  <link rel="stylesheet" href="assets/css/registrasi.css">
</head>
<!----------------------- Main Container -------------------------->
<div class="container d-flex justify-content-center align-items-center min-vh-100">
  <!----------------------- Login Container -------------------------->
     <div class="row border rounded-5 p-3 bg-white shadow box-area">
  <!--------------------------- Left Box ----------------------------->
     <div class="col-md-6 rounded-4 d-flex justify-content-center align-items-center flex-column left-box">
         <div class="featured-image mb-3">
          <img src="assets/img/BANNER PPDB STIBAJRA NEW.jpg" class="img-fluid">
         </div>
     </div> 
  <!-------------------- ------ Right Box ---------------------------->
      
     <div class="col-md-6 right-box">
        <div class="row align-items-center">
              <div class="header-text mb-4">
                   <h2>Registrasi Peserta</h2>
                   <p>Silahkan Registrasi</p>
                   <form action="/registrasi" method="POST" class="signin-form">
                    @csrf
        <div class="form-group mb-3">
            <select name="sumber_info" class="form-control @error('sumber_info')is-invalid @enderror" required value="{{ old('sumber_info') }}">
                <option value="">Sumber Informasi</option>
                <option value="Brosur">Brosur</option>
                <option value="Spanduk">Spanduk</option>
                <option value="Tetangga">Tetangga</option>
                <option value="Keluarga">Keluarga</option>
                <option value="Radio">Radio</option>
                <option value="Media Sosial">Media Sosial</option>
                <option value="Sosialisasi">Sosialisasi</option>
                <option value="Website SMK TI Bali Global Jimbaran">Website SMK TI Bali Global Jimbaran</option>
                <option value="Alumni SMK TI Bali Global Jimbaran">Alumni SMK TI Bali Global Jimbaran</option>
            </select>
            @error('sumber_info')<div class="invalid-feedback">{{ $message }}</div>@enderror
        </div>
        <div class="form-group mb-3">
          <input type="text" class="form-control @error('nama_lengkap')is-invalid @enderror" name="nama_lengkap" placeholder="Nama lengkap" required value="{{ old('nama_lengkap') }}">
          @error('nama_lengkap')<div class="invalid-feedback">{{ $message }}</div>@enderror
        </div>
        <div class="form-group mb-3">
          <input type="text" class="form-control @error('nomor_hp')is-invalid @enderror" name="nomor_hp" placeholder="Nomor telepon" required value="{{ old('nomor_hp') }}">
          @error('nomor_hp')<div class="invalid-feedback">{{ $message }}</div>@enderror
        </div>
        <div class="form-group mb-3">
         <select name="jenis_kelamin" class="form-control @error('jenis_kelamin')is-invalid @enderror" required value="{{ old('jenis_kelamin') }}">
            <option value="">Jenis Kelamin</option>
            <option value="Laki-laki">Laki-laki</option>
            <option value="Perempuan">Perempuan</option>
         </select>
         @error('jenis_kelamin')<div class="invalid-feedback">{{ $message }}</div>@enderror
        </div>
        <div class="form-group mb-3">
          <input type="text" class="form-control @error('asal_sekolah')is-invalid @enderror" name="asal_sekolah" placeholder="Asal sekolah" required value="{{ old('asal_sekolah') }}">
          @error('asal_sekolah')<div class="invalid-feedback">{{ $message }}</div>@enderror
        </div>
        <div class="form-group mb-3">
          <input type="text" class="form-control @error('email')is-invalid @enderror" name="email" placeholder="Email" required value="{{ old('email') }}">
          @error('email')<div class="invalid-feedback">{{ $message }}</div>@enderror
        </div>
        <div class="form-group mb-3">
          <input type="password" class="form-control @error('password')is-invalid @enderror" name="password" placeholder="Password" required value="{{ old('password') }}">
          @error('password')<div class="invalid-feedback">{{ $message }}</div>@enderror
        </div>

        @php
        $tanggalSekarang = now()->toDateString();

         // Gelombang Early Bird (22-30 November)
        $tanggalMulaiEarlyBird = "2023-12-01" ;
        $tanggalSelesaiEarlyBird ="2023-12-30" ;

        // Gelombang 1 (setelah tanggal 30 November)
        $tanggalMulaiGelombang1 = "2023-01-01"  ; // Mulai dari awal bulan berikutnya
        $tanggalSelesaiGelombang1 = "2023-01-30"  ; // Sampai dengan tanggal 15 bulan berikutnya

        // Gelombang 2 (setelah tanggal 5 Januari)
        $tanggalMulaiGelombang2 = "2024-02-01"  ; // Mulai dari awal bulan ke-3 berikutnya
        $tanggalSelesaiGelombang2 = "2024-02-30" ; // Sampai dengan tanggal 15 bulan ke-3 berikutnya
        @endphp

        @if ($tanggalSekarang >= $tanggalMulaiEarlyBird && $tanggalSekarang <= $tanggalSelesaiEarlyBird)
            <input type="hidden" name="gelombang_pendaftaran" value="Early Bird">
        @elseif ($tanggalSekarang > $tanggalSelesaiEarlyBird && $tanggalSekarang <= $tanggalSelesaiGelombang1)
            <input type="hidden" name="gelombang_pendaftaran" value="Gelombang 1">
        @elseif ($tanggalSekarang > $tanggalSelesaiGelombang1 && $tanggalSekarang <= $tanggalSelesaiGelombang2)
            <input type="hidden" name="gelombang_pendaftaran" value="Gelombang 2">
        @else
            <input type="hidden" name="gelombang_pendaftaran" value="Tutup Pendaftaran">
        @endif

        <div class="form-group">
            <button type="submit" class="form-control btn btn-primary rounded submit px-3">Registrasi</button>
        </div>
      </form>
              <div class="row">
                  <small>Sudah Memiliki Akun? <a href="/login">Login</a></small>
              </div>
        </div>
     </div> 
    </div>
  </div>