<?php

namespace Database\seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Sample data for seeding
        $users = [
            [
                'kode_pendaftaran' => 'KD000',
                'sumber_info' => 'Brosur',
                'nama_lengkap' => 'admin',
                'asal_sekolah' => 'STIBAJRA',
                'jenis_kelamin' => 'Laki-Laki',
                'nomor_hp' => '087820536499',
                'email' => 'admin@gmail.com',
                'password' => Hash::make('admin'),
                'gelombang_pendaftaran' => 'Early Bird',
                'is_admin' => '1',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'kode_pendaftaran' => 'KD001',
                'sumber_info' => 'Brosur',
                'nama_lengkap' => 'User',
                'asal_sekolah' => 'STIBAJRA',
                'jenis_kelamin' => 'Laki-Laki',
                'nomor_hp' => '087820536491',
                'email' => 'a@gmail.com',
                'password' => Hash::make('a'),
                'gelombang_pendaftaran' => 'Early Bird',
                'is_admin' => '0',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ];

        // Insert data into the users table
        DB::table('users')->insert($users);
    }
}
