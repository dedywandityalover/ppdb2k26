<?php

namespace App\Http\Controllers;

use App\Models\transaksi;
use App\Models\diskon;
use Dflydev\DotAccessData\Data;
use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Report\Xml\Totals;
use Illuminate\Support\Facades\Validator;

class transaksiController extends Controller
{
    public function __construct()

    {

        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

   public function indexdashboard()
    {
    // Menampilkan view dan mengirimkan data
    return view('dashboard.transaksi.index');
    }

    public function simpan(Request $request)
    {
        $request->validate([
            'id_user' => 'required',
            'id_peserta' => 'required',
            'jenis_diskon' => 'required',
            'bukti_diskon' => 'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048|min:10', // Maksimum 2 MB, minimum 10 KB
            'bukti_transfer' => 'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048|min:10', // Maksimum 2 MB, minimum 10 KB
        ]);
    
        // Simpan file bukti diskon
        if ($request->hasfile('bukti_diskon')) {
            $buktiDiskon = $request->file('bukti_diskon');
            $buktiDiskonFileName = 'bukti_diskon_' . time() . '.' . $buktiDiskon->getClientOriginalExtension();
            $buktiDiskon->move(public_path('bukti_diskon'), $buktiDiskonFileName);
        }
        else{
            $buktiDiskonFileName = "Tidak Memiliki Potongan";
        }
    
        // Simpan file bukti transfer
        if ($request->hasFile('bukti_transfer')) {
            $buktiTransfer = $request->file('bukti_transfer');
            $buktiTransferFileName = 'bukti_transfer_' . time() . '.' . $buktiTransfer->getClientOriginalExtension();
            $buktiTransfer->move(public_path('bukti_transfer'), $buktiTransferFileName);
        }
       
    
        // Simpan data diskon
        $diskon = new Diskon;
        $diskon->id_user = $request->id_user;
        $diskon->bukti_diskon = $buktiDiskonFileName; // Menggunakan nama file yang sudah di-generate
        $diskon->jenis_diskon = $request->jenis_diskon;
        $diskon->jumlah_diskon = 0;
        $diskon->save();
        
    
        // Simpan data transaksi
        $transaksi = new Transaksi;
        $transaksi->id_user = $request->id_user;
        $transaksi->id_peserta = $request->id_peserta;
        $transaksi->diskon()->associate($diskon);
        $transaksi->bukti_transfer = $buktiTransferFileName;
        $transaksi->jumlah_membayar = 0;
        $transaksi->total_membayar = 0;
        $transaksi->jumlah_biaya = $request->jumlah_biaya;
        $transaksi->jumlah_sisa = $request->jumlah_biaya;
        $transaksi->keterangan = "Akan Divalidasi Oleh Admin MAX 24 Jam";
        $transaksi->save();

        return redirect('/transaksi')->with('succes', 'File bukti diskon tidak valid');
        
    }

 

    public function updateform(Request $request)
    {
    $request->validate([
        'id_user' => 'required',
        'id_peserta' => 'required',
        'id_diskon' => 'required',
        'bukti_transfer' => 'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048|min:10', // Maksimum 2 MB, minimum 10 KB
        'jumlah_membayar' => 'required',
        'total_membayar' => 'required',
        'jumlah_biaya' => 'required',
        'jumlah_sisa' => 'required',
        'keterangan' => 'required',
    ]);

    // Handling file upload for 'bukti_transfer'
    if ($request->hasFile('bukti_transfer')) {
        $buktiTransfer = $request->file('bukti_transfer');
        $buktiTransferFileName = 'bukti_transfer_' . time() . '.' . $buktiTransfer->getClientOriginalExtension();
        $buktiTransfer->move(public_path('bukti_transfer'), $buktiTransferFileName);
    }

    // Membuat instansi Transaksi dan mengisi atribut-atributnya
    $transaksi = new Transaksi;
    $transaksi->id_user = $request->id_user;
    $transaksi->id_peserta = $request->id_peserta;
    $transaksi->id_diskon = $request->id_diskon;
    $transaksi->bukti_transfer = $buktiTransferFileName;
    $transaksi->jumlah_membayar = $request->jumlah_membayar;
    $transaksi->total_membayar = $request->total_membayar;
    $transaksi->jumlah_biaya = $request->jumlah_biaya;
    $transaksi->jumlah_sisa = $request->jumlah_sisa;
    $transaksi->keterangan = $request->keterangan;
    $transaksi->save();
    return redirect('/transaksi')->with('success', 'Data berhasil diupdate');
}

    

    function index()
    {
        $data = transaksi::with(['user', 'diskon'])->get();
        return view('admin/transaksi',compact('data'));
    }
    
    public function update(Request $request, $id)
    {
        $request->validate([
            'jumlah_membayar' => 'required',
            'total_membayar' => 'required',
            'bukti_transfer' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
        $data = transaksi::find($id);
        if ($request->hasFile('bukti_transfer')) {
        // Move the uploaded file to the destination
        $request->file('bukti_transfer')->move('bukti_transfer/', $request->file('bukti_transfer')->getClientOriginalName());

        // Update the 'bukti_diskon' field in the database
        $data->bukti_diskon = $request->file('bukti_transfer')->getClientOriginalName();
        }
        if($request->total_membayar == $data->total_membayar){
            $total_membayar = $data->total_membayar + $request->jumlah_membayar;
        }else{
            $total_membayar = $request->total_membayar;
        }
        if($data->jumlah_biaya == $data->jumlah_sisa)
        {
            $jumlah_sisa = $data->jumlah_sisa - ($request->jumlah_membayar + $data->diskon->jumlah_diskon);    
        }else{
            $jumlah_sisa = $data->jumlah_sisa - $request->jumlah_membayar;
        }
        if($jumlah_sisa < 0)
        {
            $jumlah_sisa = 0;
        }
        else{
            $jumlah_sisa = $jumlah_sisa;
        }
        $keterangan = "Tervalidasi";
        $updateData = [
            'jumlah_membayar' => $request->jumlah_membayar, // replace 'column1' and $value1 with the actual column and value you want to update
            'total_membayar' => $total_membayar,
            'jumlah_sisa' => $jumlah_sisa,
            'keterangan' => $keterangan,
        ];
    
        // Update the record using the update method
        $data->update($updateData);
        return redirect()->route('/admin/transaksi')->with('success',' Data Berhasil Di Ubah');
    }
    public function hapus($id)
    {
        $data = transaksi::find($id);
        $data->delete();
        return redirect()->route('/admin/transaksi')->with('success',' Data Berhasil Di Hapus');
    }
    public function multiDelete(Request $request) 
    {
        transaksi::whereIn('id', $request->ids)->delete();    
        return redirect()->route('/admin/transaksi')->with('success',' Data-Data Berhasil Di Hapus');
    }
}
