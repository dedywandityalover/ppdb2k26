<?php

namespace App\Http\Controllers;

use App\Charts\JumlahUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminLogin extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.login');
    }

    public function dashboard(JumlahUser $chart)
    {
        return view('admin.dashboard', ['chart' => $chart->build()]);
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'nama_lengkap' => 'required',
            'password' => 'required'
        ]);


        if(Auth::attempt($credentials)){
            // $request->session()->regenerate();
            // return redirect()->intended('/dashboard');
            if (auth()->user()->is_admin == 1) {

                return redirect('/admin/home');

            }else{
                

            }
        }
        return back()->with('loginError', 'Login Tidak Berhasil');


       
    }

    public function logout(request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/admin/login');
    }

   
}
