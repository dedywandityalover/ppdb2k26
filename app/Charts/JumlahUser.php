<?php

namespace App\Charts;

use App\Models\User;
use ArielMejiaDev\LarapexCharts\LarapexChart;

class JumlahUser
{
    protected $chart;

    public function __construct(LarapexChart $chart)
    {
        $this->chart = $chart;
    }

    public function build(): \ArielMejiaDev\LarapexCharts\LineChart
    {
        $dataBulan = [];
        $dataTotalRegis = [];

        for ($i = 1; $i <= 12; $i++) {
            $startOfMonth = new \DateTime(date('Y') . '-' . $i . '-01');
            $endOfMonth = new \DateTime(date('Y-m-t', strtotime($startOfMonth->format('Y-m-d'))));

            $totalRegis = User::where(function ($query) {
                    $query->where('is_admin', 0)
                        ->orWhereNull('is_admin');
                })
                ->whereBetween('created_at', [$startOfMonth->format('Y-m-d'), $endOfMonth->format('Y-m-d')])
                ->count();

            $dataBulan[] = $this->getIndonesianMonthName($i);
            $dataTotalRegis[] = $totalRegis;
        }

        $year = date('Y');

        return $this->chart->lineChart()
            ->setTitle("User Registrasi - $year")
            ->setSubtitle('Total User Registrasi Per Bulan')
            ->addData('Total User', $dataTotalRegis)
            ->setXAxis($dataBulan)
            ->setWidth(710)
            ->setHeight(300);
    }

    private function getIndonesianMonthName($monthNumber)
    {
        $monthNames = [
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember',
        ];

        return $monthNames[$monthNumber];
    }
}
